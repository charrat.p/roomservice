
package com.sp.repository;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sp.model.Room;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RoomRepositoryTest {
    @Autowired
    RoomRepository trepo;

    @Before
    public void setUp() {
		int idOwner = 1;
		String name = "test";
		int price = 0;

		Room h =new Room(idOwner,name,price);
        trepo.save(h );
    }

    @After
    public void cleanUp() {
        trepo.deleteAll();
    }

    @Test
    public void saveRoom() {
		int idOwner = 1;
		String name = "test";
		int price = 0;

		Room h =new Room(idOwner,name,price);
		trepo.save(h );
        assertTrue(true);
    }

    @Test
    public void saveAndGetRoom() {
        trepo.deleteAll();
        
		int idOwner = 1;
		String name = "test";
		int price = 0;

		Room h =new Room(idOwner,name,price);
		trepo.save( h);
        
		List<Room> roomList = new ArrayList<>();
        trepo.findAll().forEach(roomList::add);
        assertTrue(roomList.size() == 1);
        assertTrue(roomList.get(0).getIdOwner() == idOwner);
        assertTrue(roomList.get(0).getPrice() == price);
        assertTrue(roomList.get(0).getName().equals(name));
    }

    @Test
    public void getRoom() {
        
		int idOwner = 1;
		String name = "test";
		int price = 0;
		Room h =new Room(idOwner,name,price);
		trepo.save( h);
		
    	List<Room> roomList1 = trepo.findByName("test");
        assertTrue(roomList1.size() == 1);
        assertTrue(roomList1.get(0).getIdOwner()== idOwner);
        
    	Optional<Room> roomList2 = trepo.findById(1);
        assertTrue(roomList2.get().getPrice() == 0);
        
        
    }

    @Test
    public void findByCarte() {
    	int idOwner = 1;
		String name = "tests";
		int price = 0;
		
        trepo.save(new Room(idOwner,name,price));
        trepo.save(new Room(idOwner,"av",price));
        trepo.save(new Room(idOwner,name,price));
        trepo.save(new Room(idOwner,"test",price));
        List<Room> roomList = new ArrayList<>();
        trepo.findByName("tests").forEach(roomList::add);
        
        assertTrue(roomList.size() == 2); }
    

    
    
}
