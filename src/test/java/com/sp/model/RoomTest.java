package com.sp.model;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RoomTest {

		private List<String> nameList;
		private List<Integer> idOwnerList;
		private List<Integer> priceList;
		
		@Before
		public void setUp() {
			System.out.println("[BEFORE TEST] -- Add ROOM to test");
			nameList = new ArrayList<String>();
			priceList = new ArrayList<Integer>();
			idOwnerList = new ArrayList<Integer>();
			// Id owner 
			idOwnerList.add(0);
			idOwnerList.add(1);
			idOwnerList.add(2);
			// Name ---
			nameList.add("a");
			nameList.add("b");
			nameList.add("ab");
			// Prix 
			priceList.add(200);
			priceList.add(300);
			priceList.add(500);
			
		}
		
		@After
		public void tearDown() {
			System.out.println("[AFTER TEST] -- CLEAN carte list");
			nameList = null;
			priceList = null;
		}
		
		@Test
		public void createRoom() {
			int size=idOwnerList.size();
			for (Integer i=0; i < size;i++) {
				for (Integer msg:idOwnerList) {
					for (String msg1:nameList) {
						for (Integer msg2:priceList) {
							Room c=new Room(msg,msg1,msg2);
							assertTrue(c.getIdOwner() == msg);
							assertTrue(c.getName().equals(msg1));
							assertTrue(c.getPrice() == msg2);
						}
					}
				}
			}	
		}

		@Test
		public void displayRoom() {
			Room c= new Room(1,"pasdimage",10);
			String expectedResult= "{id:0, idOwner:1, idVisitor:null, name:pasdimage, price:10}";
			assertTrue(c.toString().equals(expectedResult));
		}
}
