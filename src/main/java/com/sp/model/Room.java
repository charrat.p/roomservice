package com.sp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Room {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Column(nullable = false)
	private Integer idOwner;
	private Integer idVisitor = null;
	private String name;
	private int price;
	
	public Room() {}
	
	/**
	 * @param id
	 * @param idOwner
	 * @param idVisitor
	 */	
	public Room(int idOwner,String name, int price) {
		super();
		this.idOwner = idOwner;
		this.name = name;
		this.price = price;
	}

	/**
	 * @return l'id de la Room
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param modifier l'id du Room
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the idOwner
	 */
	public int getIdOwner() {
		return idOwner;
	}

	/**
	 * @param idOwner the idOwner to set
	 */
	public void setIdOwner(int idOwner) {
		this.idOwner = idOwner;
	}

	/**
	 * But : Retourne l'id du visiteur de la room
	 * @return int idVisitor
	 */
	public Integer getIdVisitor() {
		return idVisitor;
	}

	/**
	 * But : Modifie l'id du visiteur de la room
	 * @param int idVisitor (issue de l'UserDTO) 
	 */
	public void setIdVisitor(int idVisitor) {
		this.idVisitor = idVisitor;
	}

	/**
	 * But : Retourne le nom de la room 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * But : Modifier le nom de la Room 
	 * @param String name 
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * But : le prix de mise de la Room
	 * @return int price  
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * @param int price 
	 * But : modifie le prix de mise de la Room 
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "{id:" + id + ", idOwner:" + idOwner + ", idVisitor:" + idVisitor + ", name:" + name + ", price:"
				+ price + "}";
	}
	
	
	
}
